DROP TABLE IF EXISTS tbLogin CASCADE;
CREATE TABLE tbLogin
(
    IdLogin BIGSERIAL NOT NULL UNIQUE PRIMARY KEY,
    Login VARCHAR(30),
    Password VARCHAR(512),
    TagValIdation VARCHAR(80),
    SocialLogin BIT
);
CREATE UNIQUE INDEX CONCURRENTLY login_index_unique ON tbLogin (Login);
ALTER TABLE tbLogin ADD CONSTRAINT login_unique UNIQUE USING INDEX login_index_unique;

DROP TABLE IF EXISTS tbPerfil CASCADE;
CREATE TABLE tbPerfil
(
    IdPerfil SERIAL NOT NULL UNIQUE PRIMARY KEY,
    Perfil VARCHAR(50),
    IsAdmin BIT
);

DROP TABLE IF EXISTS tbEnvironment CASCADE;
CREATE TABLE tbEnvironment
(
    idEnvironment SERIAL NOT NULL UNIQUE PRIMARY KEY,
    Environment VARCHAR(50),
    Mnemonic CHAR(3)
);
CREATE UNIQUE INDEX CONCURRENTLY environment_index_unique ON tbEnvironment (Environment);
ALTER TABLE tbEnvironment ADD CONSTRAINT environment_unique UNIQUE USING INDEX environment_index_unique;

DROP TABLE IF EXISTS tbPerfisDoLogin CASCADE;
CREATE TABLE tbPerfisDoLogin
(
    IdPerfil INT NOT NULL,
    IdLogin BIGINT NOT NULL,
    idEnvironment INT,
    PRIMARY KEY (IdPerfil, IdLogin, idEnvironment),
    CONSTRAINT perfil_fk FOREIGN KEY (IdPerfil) REFERENCES tbPerfil(IdPerfil),
    CONSTRAINT perfisLogin_fk FOREIGN KEY (IdLogin) REFERENCES tbLogin(IdLogin),
    CONSTRAINT environment_fk FOREIGN KEY (idEnvironment) REFERENCES tbEnvironment(idEnvironment)
);

DROP TABLE IF EXISTS tbpermissao CASCADE;
CREATE TABLE tbpermissao
(
    IdPermissao BIGSERIAL NOT NULL UNIQUE PRIMARY KEY,
    permissao VARCHAR(80)
);
CREATE UNIQUE INDEX CONCURRENTLY permissao_index_unique ON tbpermissao (permissao);
ALTER TABLE tbpermissao ADD CONSTRAINT permissao_unique UNIQUE USING INDEX permissao_index_unique;

DROP TABLE IF EXISTS tbPermissoesDoPerfil CASCADE;
CREATE TABLE tbPermissoesDoPerfil
(
    IdPerfil INT NOT NULL,
    IdPermissao BIGINT NOT NULL,
    idEnvironment INT NOT NULL,
    PRIMARY KEY (IdPerfil, IdPermissao, idEnvironment),
    CONSTRAINT Id_perfil_fk FOREIGN KEY (IdPerfil) REFERENCES tbPerfil(IdPerfil),
    CONSTRAINT Id_permissao_fk FOREIGN KEY (IdPermissao) REFERENCES tbpermissao(IdPermissao),
    CONSTRAINT Id_environment_fk FOREIGN KEY (idEnvironment) REFERENCES tbEnvironment(idEnvironment)
);

select concat('ALTER TABLE ', kcu.table_name, ' CLUSTER ON ', tco.constraint_name, ';'),
       kcu.table_schema,
       kcu.table_name,
       tco.constraint_name,
       kcu.ordinal_position as position,
       kcu.column_name as key_column
from information_schema.table_constraints tco
join information_schema.key_column_usage kcu
     on kcu.constraint_name = tco.constraint_name
     and kcu.constraint_schema = tco.constraint_schema
     and kcu.constraint_name = tco.constraint_name
where tco.constraint_type = 'PRIMARY KEY'
order by kcu.table_schema,
         kcu.table_name,
         position;

--DATABASE SECURITY PARA USER APPLICATION
GRANT CONNECT ON DATABASE security TO application;
GRANT CONNECT ON DATABASE security TO application;
GRANT SELECT, INSERT, UPDATE, DELETE, TRUNCATE, REFERENCES, TRIGGER ON ALL TABLES IN SCHEMA public TO application;
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO application;


GRANT ALL PRIVILEGES ON DATABASE security TO adm;


INSERT INTO tbperfil VALUES (DEFAULT, 'Admin', '1');

INSERT INTO tblogin VALUES (DEFAULT, 'Andre Maximianos', '123', 'Tag', '1');

INSERT INTO tbenvironment VALUES (DEFAULT, 'Prod', 'PRD');
INSERT INTO tbenvironment VALUES (DEFAULT, 'Dev', 'DEV');
INSERT INTO tbenvironment VALUES (DEFAULT, 'Hom', 'HOM');

INSERT INTO tbperfisdologin VALUES (1, 1, 1);

INSERT INTO tbpermissao VALUES (DEFAULT, 'token-security');
INSERT INTO tbpermissao VALUES (DEFAULT, 'token-toggle');

INSERT INTO tbpermissoesdoperfil VALUES (1, 1, 1);
INSERT INTO tbpermissoesdoperfil VALUES (1, 2, 1);
INSERT INTO tbpermissoesdoperfil VALUES (1, 1, 2);
INSERT INTO tbpermissoesdoperfil VALUES (1, 2, 2);
INSERT INTO tbpermissoesdoperfil VALUES (1, 1, 3);
INSERT INTO tbpermissoesdoperfil VALUES (1, 2, 3);







